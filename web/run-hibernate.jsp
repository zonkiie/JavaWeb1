<%@page import="javax.persistence.*"%>
<%@page import="javax.persistence.criteria.*"%>
<%@page import="java.io.*"%>
<%@page import="java.time.*"%>
<%@page import="java.util.*"%>
<%@page import="java.lang.*"%>
<%@page import="org.hibernate.*"%>
<%@page import="com.myapp.struts.*"%>
<%@page import="com.myapp.struts.entities.*" %>
<%@page import="javax.servlet.http.HttpUtils.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@include file="header.jsp" %>
    <body>
        <h1>Run Hibernate Test</h1>
        <%
            class JSPTest1 {

                public Session hibernateSession;
                public Transaction tx;

                public JSPTest1 CreateSession() {
                    hibernateSession = new HibernateHelper().getSession();
                    tx = hibernateSession.beginTransaction();
                    return this;
                }

                public JSPTest1 DestroySession() {
                    tx.commit();
                    hibernateSession.getSessionFactory().close();
                    hibernateSession.close();
                    return this;
                }

                public JSPTest1 CreateRandomEntry() {
                    // hibernateSession.saveOrUpdate(new Task("User1", "Task1"));
                    Integer randi = new Random().nextInt(100000);
                    hibernateSession.saveOrUpdate(new Task("User" + randi, "Task" + randi));
                    hibernateSession.flush();
                    return this;
                }
                
                public JSPTest1 CreateEntry(String User, String Task) {
                    hibernateSession.saveOrUpdate(new Task(User, Task));
                    hibernateSession.flush();
                    return this;
                }

                public JSPTest1 DeleteEntry(UUID id) {
                    Task t = hibernateSession.load(Task.class, id);
                    hibernateSession.delete(t);
                    return this;
                }
                
                public JSPTest1 UpdateEntry(Task task) {
                    hibernateSession.saveOrUpdate(task);
                    hibernateSession.flush();
                    return this;
                }
                
                public Task GetEntry(UUID id) {
                    return hibernateSession.load(Task.class, id);
                }

                public List<Task> GetEntries() throws IOException {
                    CriteriaBuilder cb = hibernateSession.getCriteriaBuilder();
                    CriteriaQuery<Task> query = cb.createQuery(Task.class);
                    Root<Task> variableRoot = query.from(Task.class);
                    query.select(variableRoot);
                    return hibernateSession.createQuery(query).getResultList();
                    //return (List<Task>) hibernateSession.createCriteria(Task.class).list();
                }

            }
            Task task = null;
            JSPTest1 j = new JSPTest1();
            j.CreateSession();
            if(request.getParameter("create_random") != null && !request.getParameter("create_random").equals("")) j.CreateRandomEntry();
            if(request.getParameter("save") != null && !request.getParameter("save").equals(""))
            {
                if(request.getParameter("User") != null && request.getParameter("Task") != null) j.CreateEntry(request.getParameter("User"), request.getParameter("Task"));
            }
            if(request.getParameter("remove") != null && !request.getParameter("remove").equals("") )
            {
                if(request.getParameter("ID") != null && !request.getParameter("ID").equals(""))
                {
                    UUID id = UUID.fromString(request.getParameter("ID"));
                    j.DeleteEntry(id);
                }
            }
            if(request.getParameter("edit") != null && !request.getParameter("edit").equals("") )
            {
                if(request.getParameter("ID") != null && !request.getParameter("ID").equals(""))
                {
                    UUID id = UUID.fromString(request.getParameter("ID"));
                    task = j.GetEntry(id);
                }
            }
            if(request.getParameter("edit-commit") != null && !request.getParameter("edit-commit").equals("") )
            {
                if(request.getParameter("ID") != null && !request.getParameter("ID").equals(""))
                {
                    UUID id = UUID.fromString(request.getParameter("ID"));
                    task = j.GetEntry(id);
                    task.setUsername(request.getParameter("User"));
                    task.setTaskname(request.getParameter("Task"));
                    j.UpdateEntry(task);
                }
            }
            List<Task> tasklist = j.GetEntries();
            if(tasklist.size() > 0)
            {
                //out.print("Tasklist:" + tasklist);
                %>
                <TABLE CLASS="table">
                    <tr><th>ID</th><th>User</th><th>Task</th><th>Action</th></tr>
                    <%
                    for(Task t: tasklist) {%>
                    <tr><td><%=t.getID().toString()%></td><td><%=t.getUsername()%></td><td><%=t.getTaskname()%></td><td><FORM METHOD="POST"><INPUT TYPE="submit" NAME="edit" VALUE="Bearbeiten"> <INPUT TYPE="submit" NAME="remove" VALUE="Entfernen"><INPUT TYPE="hidden" NAME="ID" VALUE="<%=t.getID().toString()%>"></FORM></td></tr>
                    <%}%>
                </TABLE>
                <%
            }
            j.DestroySession();

        %><br>

        <FORM METHOD="POST">
            <% if(task != null && task.getID() != null) { %> <INPUT TYPE="hidden" NAME="ID" VALUE="<%=task.getID().toString()%>"> <%}%>
            <TABLE CLASS="table">
                <TR><TD>Username</TD><TD><INPUT type="text" name="User" <% if(task != null) {%> VALUE="<%=task.getUsername()%>"  <%}%>></TD></TR>
                <TR><TD>Task</TD><TD><INPUT type="text" name="Task" <% if(task != null) {%> VALUE="<%=task.getTaskname()%>"  <%}%>></TD></TR>
                <TR><TD colspan="2"><INPUT type="submit" NAME="<% if(task == null) out.print("save"); else out.print("edit-commit");%>" value="Speichern"> <INPUT TYPE="submit" NAME="create_random" VALUE="Zufalls-Eintrag erzeugen"> <INPUT type="reset" value="Reset"></TD></TR>
            </TABLE>
        </FORM>
        Timestamp: <% out.print(java.time.LocalDateTime.now().toString());%><BR>
        <A HREF="<%= request.getRequestURL() %>">This Page</A><BR>
        <A HREF="index.htm">Main Page</A><BR>
    </body>
</html>
