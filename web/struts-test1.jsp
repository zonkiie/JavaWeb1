<%-- 
    Document   : struts-hibernate
    Created on : 02.06.2018, 14:43:09
    Author     : rainer
--%>
<%--https://dzone.com/tutorials/java/struts/struts-example/struts-login-page-example-1.html--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="struts-header.jspf" %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <%--<html:form action="/hello">--%>
        <html:form action="/handle_action1">
            <html:text property="username" name="ActionForm1" />
          <html:submit value="Submit" />
        </html:form>
        <H3>Your Message:</H3>
        <span><bean:write name="ActionForm1" property="message" scope="request"/></span><br>
        <span><bean:write name="ResultMessage" ignore="true"/></span><br>
        <A HREF="index.htm">Main Page</A><BR>
    </body>
</html>
