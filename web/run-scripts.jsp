<%@page import="java.io.*"%>
<%@page import="java.time.*"%>
<%@page import="java.util.*"%>
<%@page import="java.lang.*"%>
<%@page import="com.myapp.struts.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Run scripts!</h1>
        Script Engine Factories:<br>
        <%
            out.print(Test1.GetScriptEngineFactories().replaceAll("\n", "<br>"));
            File directory = new File(getServletContext().getRealPath("."));
            out.print(Test1.RunPhp(directory.getAbsolutePath() + "/example.php"));
        %><br>
        Timestamp: <% out.print(java.time.LocalDateTime.now().toString()); %><BR>
        <A HREF="index.htm">Main Page</A><BR>
    </body>
</html>
