package com.myapp.struts;

import javax.servlet.http.*;
import org.apache.struts.action.*;

/**
 *
 * @author rainer
 */
public class Action1 extends Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
        ActionForm1 aform1 = (ActionForm1) form;
        //System.out.println("We are executing the form. Username:" + aform1.getUsername());
        if (aform1.getUsername() == null) {
            return mapping.findForward("failure");
        } else {
            String Message = "We have successfully executed the form. Username:" + aform1.getUsername();
            aform1.setMessage(Message);
            request.setAttribute("ActionForm1", aform1);
            request.setAttribute("ResultMessage", Message);
            //System.out.println(aform1.getMessage());
            return mapping.findForward("success");
        }
    }

}
