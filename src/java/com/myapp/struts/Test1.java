/*
* @see http://php-java-bridge.sourceforge.net/doc/examples/source.php?source=PhpThreads.java
*/
package com.myapp.struts;

import java.io.*;
import javax.script.*;
import java.security.cert.*;
import java.util.*;

public class Test1 {
    

    public static String GetScriptEngineFactories() {
        StringBuilder resBuilder = new StringBuilder();
        ScriptEngineManager manager = new ScriptEngineManager();
        List<ScriptEngineFactory> engineFactories = manager.getEngineFactories();
        for(ScriptEngineFactory sef:engineFactories) resBuilder.append(sef.getEngineName() + ":" + sef.getLanguageName() + "\n");
        return resBuilder.toString();
    }

    public static String RunPhp(String Filename) throws ScriptException, FileNotFoundException {
        FileReader fr = new FileReader(Filename);
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("php");
        ByteArrayOutputStream sharedResource = new ByteArrayOutputStream();
        if(engine == null) throw new ScriptException("Engine not found!");
        //engine.put("nr", new Integer(i + 1));
        engine.put("sharedResource", sharedResource);
        Object result = engine.eval(fr);
        return sharedResource.toString();
    }

}
