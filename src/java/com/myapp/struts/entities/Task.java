package com.myapp.struts.entities;

import java.io.Serializable;
import java.util.*;
import javax.persistence.*;
import javax.transaction.*;
import org.hibernate.annotations.Type;
//import org.hibernate.service.Service;

/**
 *
 * @author rainer
 */
@Entity
@Table(name = "public.task", indexes = {
    @Index(columnList = "\"ID\"", name = "pkey_task"),
    @Index(columnList = "\"Username\",\"Taskname\"", name = "unique_username_task", unique = true)
    }
)
public class Task implements Serializable {

    @Id
    @Type(type = "pg-uuid")
    @Column(columnDefinition = "UUID DEFAULT uuid_generate_v4()", name = "\"ID\"")
    java.util.UUID ID = UUID.randomUUID();
    @Column(name = "\"Username\"")
    String Username;
    @Column(name = "\"Taskname\"")
    String Taskname;

    public Task() {
    }

    public Task(String Username, String Taskname) {
        this.Username = Username;
        this.Taskname = Taskname;
    }
    
    public UUID getID() { return this.ID; }
    public String getUsername() { return this.Username; }
    public String getTaskname() { return this.Taskname; }
    public Task setUsername(String Username)  { this.Username = Username; return this; }
    public Task setTaskname(String Taskname)  { this.Taskname = Taskname; return this; }
    
    public String toString()
    {
        return ID + ";" + Username + ";" + Taskname;
    }
}
