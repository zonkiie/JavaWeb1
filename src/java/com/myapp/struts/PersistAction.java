/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts;

import java.io.*;
import java.util.*;
import java.lang.*;
import java.lang.reflect.*;
import javax.servlet.http.*;
import com.myapp.struts.entities.*;
import org.apache.struts.action.*;
import org.apache.commons.beanutils.*;
import org.apache.commons.lang3.builder.*;
import org.hibernate.*;

/**
 *
 * @author rainer
 */
public class PersistAction extends Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        DynaActionForm dform = (DynaActionForm) form;
        String ClassName = "com.myapp.struts.entities." + dform.getString("ClassName");
        Object SaveObject = Class.forName(ClassName).newInstance();
        BeanUtilsBean2 bu = new BeanUtilsBean2();
        Session session = new HibernateHelper().getSession();

        for (String key : (Set<String>) dform.getMap().keySet()) {
            if ("ClassName".equals(key)) {
                continue;
            }
            Field f = SaveObject.getClass().getDeclaredField(key);
            f.setAccessible(true);
            f.set(SaveObject, dform.get(key));
            f.setAccessible(false);
            //bu.setProperty(SaveObject, key, dform.get(key));
        }

        //System.out.println("SaveObject:" + ToStringBuilder.reflectionToString(SaveObject));
        try {
            Transaction tx = session.beginTransaction();
            session.saveOrUpdate(SaveObject);
            session.flush();
            tx.commit();
            session.getSessionFactory().close();
            session.close();
            request.setAttribute("ResultMessage", "Success");
            return mapping.findForward("success");

        } catch (Exception ex) {
            session.getSessionFactory().close();
            session.close();
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            ex.printStackTrace(printWriter);
            request.setAttribute("ResultMessage", "Failure:" + stringWriter.toString());
            return mapping.findForward("failure");
        }
    }
}
