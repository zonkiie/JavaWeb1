/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.myapp.struts;

import org.apache.struts.action.*;

/**
 *
 * @author rainer
 */
public class ActionForm1 extends ActionForm {
    private String Username;
    private String Message;

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        this.Username = username;
    }
    
    public String getMessage() {
        return Message;
    }
    
    public void setMessage(String message) {
        this.Message = message;
    }
    
    public void execute()
    {
        System.err.println(Username);
    }
}
