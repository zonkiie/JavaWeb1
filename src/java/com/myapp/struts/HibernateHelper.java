package com.myapp.struts;

import java.util.*;

import org.hibernate.*;
import org.hibernate.cfg.*;
import com.myapp.struts.entities.*;
import javax.persistence.Persistence;
import org.hibernate.boot.*;
import org.hibernate.boot.model.naming.*;
import org.hibernate.boot.registry.*;
import org.hibernate.jpa.boot.internal.*;
import com.fasterxml.classmate.*;
import javax.enterprise.inject.spi.*;

/**
 *
 * @see https://www.tutorialspoint.com/hibernate/hibernate_annotations.htm
 * @see http://docs.jboss.org/hibernate/orm/5.3/userguide/html_single/Hibernate_User_Guide.html#appendix-legacy-bootstrap
 */
public class HibernateHelper {

    private SessionFactory sessionFactory;
    private Session session;

    private void CreateHibernateSession() {
        StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
        Metadata metadata = new MetadataSources(standardRegistry).getMetadataBuilder()
                .applyImplicitNamingStrategy(ImplicitNamingStrategyJpaCompliantImpl.INSTANCE)
                .build();

        SessionFactoryBuilder sessionFactoryBuilder = metadata.getSessionFactoryBuilder();
        
        sessionFactoryBuilder.applyBeanManager(CDI.current().getBeanManager());
        // Apply a CDI BeanManager ( for JPA event listeners )
        //sessionFactoryBuilder.applyBeanManager(getBeanManager());
        sessionFactoryBuilder.enableReleaseResourcesOnCloseEnabled(true);
        sessionFactoryBuilder.applyAutoClosing(true);

        sessionFactory = sessionFactoryBuilder.build();
        session = sessionFactory.openSession();
    }
    
    public HibernateHelper() {
        CreateHibernateSession();
    }

    public Session getSession() {
        return session;
    }
}
